#!/usr/bin/env python
# coding: utf-8

# In[27]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


# In[28]:


climatedata = pd.read_csv('Change_Climate.csv')
climatedata


# In[29]:


X = climatedata.iloc[:,0:1].values
y = climatedata.iloc[:,1].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)


# In[30]:


X_train, X_test, y_train, y_test


# In[31]:


from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)


# In[32]:


def show_linear():
    plt.scatter(X, y, color='orange')
    plt.plot(X, lin_reg.predict(X), color='blue')
    plt.title('Linear Regression')
    plt.xlabel('Year')
    plt.ylabel('Temperature')
    plt.show()
    return
show_linear() 


# In[33]:


lin_reg.predict([[5.5]])


# In[34]:


from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree=4)
X_poly = poly_reg.fit_transform(X)
pol_reg = LinearRegression()
pol_reg.fit(X_poly, y)


# In[35]:


def show_polymonial():
    plt.scatter(X, y, color='red')
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
    plt.title('Linear Regression')
    plt.xlabel('Year')
    plt.ylabel('Temperature')
    plt.show()
    return

show_polymonial()


# In[36]:


pol_reg.predict(poly_reg.fit_transform([[5.5]]))


# In[37]:


predicted = {}
for i in range(1,30):
    yhat = -53.21 + 0.0371 * (2000 + i)
    predicted[2003 + i] = yhat


# In[38]:


predicted

