try:
    import pymongo #import libraries
    import csv
    import pandas as pd
    import json
except:
    print("Error while importing library")
finally:
    print("checked!")
mongoClient = pymongo.MongoClient("mongodb://localhost:27017/") 
mDatabase = mongoClient["temperature"] #database name
dbCollection = mDatabase["csv"] #collection name
def insertDatatoDb(): #function to insert data to mongoDB
    try:
        df = pd.read_csv('annual.csv',encoding = 'ISO-8859-1', delimiter='.')  
        df.to_json('_temp.json')                              
        jdf = open('_temp.json').read()                      
        data = json.loads(jdf)
        res= dbCollection.insert_one(data)
    except:
        print("Error while reading file")
    finally:
        print("checked reading file!")
        print("file inserted to database: ", res.inserted_id)  

insertDatatoDb()

