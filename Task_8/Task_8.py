#!/usr/bin/env python
# coding: utf-8

# In[60]:


from sklearn import tree
import pydotplus
import pandas as pd
from IPython.display import Image


# In[61]:


temperature_annual = pd.read_csv('Change_Climate.csv')
temperature_annual


# In[62]:


dummi_data = temperature_annual.iloc[:, 0:4].values
dummi_data


# In[63]:


temperature_file = temperature_annual[['Year', 'Temperature departure','Warmest year ranking',
                                                   'Climate','Dividing Climate']]
temperature_file


# In[64]:


temperature_data = pd.get_dummies(temperature_file)
temperature_data


# In[65]:


clf = tree.DecisionTreeClassifier()
clf_train = clf.fit(temperature_data, temperature_annual['Climate'])
clf_train = clf.fit(temperature_data, temperature_annual['Dividing Climate'])


# In[66]:


dot_data = tree.export_graphviz(clf_train, out_file=None, feature_names=list(temperature_data.columns.values), 
                                class_names=['Cold', 'Normal cool', 'Hot', 'High cool', 'Normal hot', 
                                             'High hot'],
                                rounded=True, filled=True)

graph = pydotplus.graph_from_dot_data(dot_data)
Image(graph.create_png())

