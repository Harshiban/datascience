#!/usr/bin/env python
# coding: utf-8

# In[8]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
from sklearn.cluster import KMeans


# In[ ]:


data = pd.read_csv('read.csv')


# In[16]:


data


# In[21]:


x = data.iloc[:,1:2].values
y = data.iloc[:,2].values


# In[22]:


y


# In[38]:


plt.scatter(data['Year'],data['Mean'])
# Set limits of the axes, again to resemble the world map
plt.xlim(1850,2050)
plt.ylim(-1,1.5)
plt.show


# In[39]:


x = data.iloc[:,1:2].values
x


# In[40]:


kmeans = KMeans(3)
kmeans.fit(x)


# In[41]:


identifiedcluster = kmeans.fit_predict(x)
identifiedcluster


# In[42]:


data_with_clusters = data.copy()
data_with_clusters['Cluster'] = identifiedcluster
data_with_clusters


# In[44]:


plt.scatter(data_with_clusters['Year'],data_with_clusters['Mean'],c=data_with_clusters['Cluster'],cmap='rainbow')
plt.xlim(1850,2050)
plt.ylim(-2,2)
plt.show()


# In[ ]:




